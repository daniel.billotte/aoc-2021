"""
This is part II of day #1 of the Advent of code, 2021.

Sliding Windows
"""
import sys


def readings():
    r0 = sys.stdin.readline()
    r1 = sys.stdin.readline()
    for raw_reading in sys.stdin:
        reading = int(raw_reading.strip())
        out = r0 + r1 + reading
        r0, r1 = r1, reading
        yield out


count = 0
last_reading = 30000
for reading in readings():
    count += int(reading > last_reading)
    last_reading = reading
print(count)
