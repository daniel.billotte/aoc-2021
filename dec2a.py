"""
Dec 2, 2021

Enable Navigation
"""
import re
import sys
from functools import reduce


def line_to_struct(stream, f=None):
    return (line if f == None else f(line) for line in stream if line != "\n")


def parse_heading(line):
    match = re.search(r"^(forward|up|down) (\d+)\s*$", line)
    return (match.group(1), int(match.group(2)))


def process_heading(state, heading):
    if heading[0] == "forward":
        state["forward_distance"] += heading[1]
    elif heading[0] == "up":
        state["depth"] -= heading[1]
    elif heading[0] == "down":
        state["depth"] += heading[1]
    return state


def process_headings(headings):
    position_delta = reduce(
        process_heading, headings, {"depth": 0, "forward_distance": 0}
    )
    return position_delta["depth"] * position_delta["forward_distance"]


print(process_headings(line_to_struct(sys.stdin, parse_heading)))
