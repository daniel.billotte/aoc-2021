"""
Dec 3, 2021

Power Consumption

This implementation only works for an input that is 
12 bits wide. See lines 23, 31, &45 to adjust
"""
import re
import sys
from functools import reduce

BIT_WIDTH = 12

def line_to_struct(stream, f=None):
    return (line if f == None else f(line) for line in stream if line != "\n")



"""
return the bitmask for the given bit (zero-based)
"""
def mask(idx):
        return reduce(lambda acc, item: acc << 1, range((BIT_WIDTH -1) - idx), 1)


def parse_power_reading(line):
    return int(line.strip(), 2)


def process_reading(gamma_bits, reading):
    for bit in range(BIT_WIDTH):
        if reading & mask(bit):
            gamma_bits[bit] +=1
        else:
            gamma_bits[bit] -=1

    return gamma_bits


def lshift_on(bits, bit):
    return (bits << 1) + (1 if bit else 0)


def calculate_gamma_epsilon(readings):
    gamma_bits = [0 for i in range(BIT_WIDTH)]
    raw_bits = reduce(process_reading, readings, gamma_bits)
    gamma_bits = [1 if bit > 0 else 0 for bit in raw_bits]
    epsilon_bits = [1 if bit <= 0 else 0 for bit in raw_bits]
    return (reduce(lshift_on, gamma_bits, 0), reduce(lshift_on, epsilon_bits, 0))


readings = line_to_struct(sys.stdin, parse_power_reading)
gamma, epsilon = calculate_gamma_epsilon(readings)
power_consumption = gamma * epsilon

print(gamma, epsilon)
print(bin(gamma), bin(epsilon))
print(power_consumption)

