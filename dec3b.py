"""
Dec 3, 2021

Power Consumption

This implementation only works for an input that is 
12 bits wide. See lines 23, 31, &45 to adjust
"""
import re
import sys
from functools import reduce

BIT_WIDTH = 12

def line_to_struct(stream, f=None):
    return (line if f == None else f(line) for line in stream if line != "\n")



"""
return the bitmask for the given bit (zero-based)
"""
def mask(idx):
        return reduce(lambda acc, item: acc << 1, range((BIT_WIDTH -1) - idx), 1)


def parse_power_reading(line):
    return int(line.strip(), 2)


"""
for the given bit number see which value (0 or 1) is
the most popular in the list. 
returns
  1 if 1 is most popular
  0 if 0 is most popular
  -1 if it is a tie
"""
def popular_bit(readings, bit_num):
    count = 0
    bitmask = mask(bit_num)
    for reading in readings:
        if reading & bitmask:
            count += 1
        else:
            count -= 1
    if count > 0:
        return 1
    elif count == 0:
        return -1
    else:
        return 0


def f(reading, value, bitmask, tie):
    if value == -1:
        if tie == 0:
            return not (reading & bitmask)
        else:
            return reading & bitmask
    elif value == 1:
        return reading & bitmask
    else:
        return not (reading & bitmask)
       

def filter_popular_bit(readings, bit_num, value, tie):
    bitmask = mask(bit_num)
    return filter(lambda reading: f(reading, value, bitmask, tie), readings)


def find_o2_rating(readings):
    filtered = readings
    for bit_num in range(BIT_WIDTH):
        pop_bit = popular_bit(filtered, bit_num)
        filtered = list(filter_popular_bit(filtered, bit_num, pop_bit, 1))
        if len(filtered) == 1:
            break
    return filtered[0]


def find_co2_scrubber_rating(readings):
    filtered = readings
    for bit_num in range(BIT_WIDTH):
        pop_bit = popular_bit(filtered, bit_num)
        if pop_bit == 1:
            pop_bit = 0
        elif pop_bit == 0:
            pop_bit = 1
        filtered = list(filter_popular_bit(filtered, bit_num, pop_bit, 0))
        if len(filtered) == 1:
            break
    return filtered[0]


readings = list(line_to_struct(sys.stdin, parse_power_reading))
# print(readings) 

o2_rating = find_o2_rating(readings)
print("o2 generator rating: ", o2_rating)

co2_rating = find_co2_scrubber_rating(readings)
print("co2 scrubber rating: ", co2_rating)

life_support_rating = o2_rating * co2_rating
print("life support rating: ", life_support_rating)

