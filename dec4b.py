from sys import stdin
from functools import reduce

class Cell:
    value = 0
    marked = False
    def __init__(self, value):
        self.value = int(value)

    def __repr__(self):
        return str(self.value) if not self.marked else "(" + str(self.value) + ")"

def line_to_struct(stream, f=None):
    return (line if f == None else f(line) for line in stream if line != "\n")

def read_boards(stream):
    boards = []
    board = []
    for line in stream.readlines():
        if line.strip() == "":
            if len(board) == 0:
                continue
            boards.append(board)
            board = []
            continue
        cell_values = filter(lambda cell: cell != "", line.strip().split(" "))
        board += [Cell(cell_val) for cell_val in cell_values]
    return boards


def any_true(items):
    return reduce(lambda acc, item: acc or item, items, False)


def all_cells_marked(cells):
    return reduce(lambda acc, cell: acc and cell.marked, cells, True)


def board_is_winner(board):
    return any_true(
        [all_cells_marked(board[idx::5]) for idx in range(5)] \
            + [all_cells_marked(board[idx:idx+5]) for idx in range(0,25,5)]
    )

def score_board(board):
    return reduce(lambda acc, cell: acc + cell.value,
        filter(lambda cell: not cell.marked, board), 0)
    
def mark_board(board, value):
    for cell in board:
        if cell.value == value:
            cell.marked = True

draw_numbers = [int(num) for num in stdin.readline().strip().split(",")]

boards = read_boards(stdin)

winning_board = None
last_number = 0
boards_to_remove = []
for draw_number in draw_numbers:
    # print("Next Number: ", draw_number)
    for board in boards:
        mark_board(board, draw_number)
        if board_is_winner(board):
            if len(boards) == 1:
                winning_board = board
                last_number = draw_number
                break
            else:
                boards_to_remove.append(board)
    if winning_board != None:
        break

    for board in boards_to_remove:
        boards.remove(board)
    boards_to_remove = []
            


board_score = score_board(winning_board)
print("board_score: ", board_score)
print("last draw number", last_number)
print("total score: ", board_score * last_number)



# print(boards)