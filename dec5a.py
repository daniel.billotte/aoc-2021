"""

"""

import re
from sys import stdin

class Line:
    def __init__(self, x0, y0, x1, y1):
        if x0 == x1:
            # vertical
            self.vertical = True
            self.x0 = self.x1 = x0
            if y0 < y1:
                self.y0 = y0
                self.y1 = y1
            else:
                self.y0 = y1
                self.y1 = y0
        else:
            # horizontal
            self.vertical = False
            self.y0 = self.y1 = y0
            if(x0 < x1):
                self.x0 = x0
                self.x1 = x1
            else:
                self.x0 = x1
                self.x1 = x0

    def __repr__(self):
        return "("+str(self.x0)+","+str(self.y0)+")->("+str(self.x1)+","+str(self.y1)+")"


def add_point(array, x, y):
    y_row = sparse_array.get(y)
    if not y_row:
        y_row = {x: 1}
        array[y] = y_row
    else:
        y_row[x] = y_row.get(x, 0) + 1

# def print_array(array):
#     for row in range(max(array.keys())):
#         print

def add_line(array, line):
    if(line.vertical):
        for y in range(line.y0, line.y1+1):
            add_point(array, line.x0, y)
    else:
        for x in range(line.x0, line.x1+1):
            add_point(array, x, line.y0)


def line_to_struct(stream, f=None):
    return (line if f == None else f(line) for line in stream if line != "\n")


def parse_line(line):
    match = re.search(r"(\d+),(\d+)\D+(\d+),(\d+)", line.strip())
    if match:
        x0 = match.group(1)
        y0 = match.group(2)
        x1 = match.group(3)
        y1 = match.group(4)
        if (x0 == x1) or (y0 == y1):
            return Line(int(x0), int(y0), int(x1), int(y1))
    return None

lines = list(filter(lambda line: line != None, line_to_struct(stdin, parse_line)))
sparse_array = {}
for line in lines:
    add_line(sparse_array, line)

count = 0
for row in sparse_array.values():
    for value in row.values():
        if value > 1:
            count += 1

print(count)


# print(sparse_array)
