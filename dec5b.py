import re
from sys import stdin

class Line:
    def assign(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

        
    def __init__(self, x0, y0, x1, y1):
        if x0 == x1:
            self.type = "vert"
            if y0 < y1:
                self.assign(x0, y0, x1, y1)
            else:
                self.assign(x1, y1, x0, y0)
        else:
            if(x0 < x1):
                self.assign(x0, y0, x1, y1)
            else:
                self.assign(x1, y1, x0, y0)

            if y0 == y1:
                self.type = "horiz"
            elif (x1 - x0) == (y1 - y0):
                self.type = "45-1"
            else:
                self.type = "45-2"
        

    def __repr__(self):
        return "("+str(self.x0)+","+str(self.y0)+")->("+str(self.x1)+","+str(self.y1)+")"


def add_point(array, x, y):
    y_row = array.get(y)
    if not y_row:
        y_row = {x: 1}
        array[y] = y_row
    else:
        y_row[x] = y_row.get(x, 0) + 1


def add_line(array, line):
    if line.type == "vert":
        for y in range(line.y0, line.y1+1):
            add_point(array, line.x0, y)
    elif line.type == "horiz":
        for x in range(line.x0, line.x1+1):
            add_point(array, x, line.y0)
    elif line.type == "45-1":
        delta = line.y0 - line.x0
        for x in range(line.x0, line.x1+1):
            add_point(array, x, x + delta)
    else:
        delta = line.x0 + line.y0
        for x in range(line.x0, line.x1+1):
            add_point(array, x, delta - x)

def line_to_struct(stream, f=None):
    return (line if f == None else f(line) for line in stream if line != "\n")


def parse_line(line):
    match = re.search(r"(\d+),(\d+)\D+(\d+),(\d+)", line.strip())
    if match:
        x0 = int(match.group(1))
        y0 = int(match.group(2))
        x1 = int(match.group(3))
        y1 = int(match.group(4))
        if (x0 == x1) or (y0 == y1):
            return Line(x0, y0, x1, y1)
        if (x1 - x0) == (y1 - y0) or (x1 - x0) == -(y1 - y0):
            return Line(x0, y0, x1, y1)
        
    return None

lines = list(filter(lambda line: line != None, line_to_struct(stdin, parse_line)))
sparse_array = {}
for line in lines:
    add_line(sparse_array, line)

count = 0
for row in sparse_array.values():
    for value in row.values():
        if value > 1:
            count += 1

print(count)


